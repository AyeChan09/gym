import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightLossFoodPage } from './weight-loss-food';

@NgModule({
  declarations: [
    WeightLossFoodPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightLossFoodPage),
  ],
})
export class WeightLossFoodPageModule {}
