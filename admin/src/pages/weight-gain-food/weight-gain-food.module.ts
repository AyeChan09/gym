import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightGainFoodPage } from './weight-gain-food';

@NgModule({
  declarations: [
    WeightGainFoodPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightGainFoodPage),
  ],
})
export class WeightGainFoodPageModule {}
