import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-weight-gain-food',
  templateUrl: 'weight-gain-food.html',
})
export class WeightGainFoodPage {
data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public storage: LocalStorageProvider) {
      this.apiService.getData_local('assets/json/weightGainFoodData.json').subscribe(data => {
        this.data = data.data;
        console.log(this.data);
        this.addTick();
      });
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightGainFoodPage');
  }
  addTick() {
    this.storage.getData('weight_gain_tick').then(data => {
      let tickArr = data;
      console.log('tickArr=>', tickArr);
      if(tickArr == null) {
        this.data.forEach(element => {
          element['tick'] = false;
        })
      } else {
        this.data = tickArr;
      }
      console.log('data in addTick=>', this.data);
    });
  }
  goWeightGainFoodDetails(data) {
    this.storage.setData('weight_gain_tick', this.data);
    // localStorage.setItem('weight_gain_tick', this.data);
    this.navCtrl.push("WeightGainFoodDetailsPage", data);
  }
  ionViewWillLeave()
 {
   console.log('data in leave=>', this.data);
   this.storage.setData('weight_gain_tick', this.data);
    console.log('leave');
 }
}
