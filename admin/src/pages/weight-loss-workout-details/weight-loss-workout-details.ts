import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-weight-loss-workout-details',
  templateUrl: 'weight-loss-workout-details.html',
})
export class WeightLossWorkoutDetailsPage {
  data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
      this.data = navParams.data;
      console.log(this.data);
      console.log(navParams.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightLossWorkoutDetailsPage');
  }

}
