import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightLossWorkoutDetailsPage } from './weight-loss-workout-details';

@NgModule({
  declarations: [
    WeightLossWorkoutDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightLossWorkoutDetailsPage),
  ],
})
export class WeightLossWorkoutDetailsPageModule {}
