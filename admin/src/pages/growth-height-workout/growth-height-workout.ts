import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-growth-height-workout',
  templateUrl: 'growth-height-workout.html',
})
export class GrowthHeightWorkoutPage {
  data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider) {
      this.apiService.getData_local('assets/json/growthHeightData.json').subscribe(data => {
        this.data = data.data;
        console.log(this.data);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GrowthHeightWorkoutPage');
  }

  goGrowthHeightWorkoutDetails(data) {
    this.navCtrl.push("GrowthHeightWorkoutDetailsPage", data);
  }

}
