import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrowthHeightWorkoutPage } from './growth-height-workout';

@NgModule({
  declarations: [
    GrowthHeightWorkoutPage,
  ],
  imports: [
    IonicPageModule.forChild(GrowthHeightWorkoutPage),
  ],
})
export class GrowthHeightWorkoutPageModule {}
