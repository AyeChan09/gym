import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-growth-height',
  templateUrl: 'growth-height.html',
})
export class GrowthHeightPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GrowthHeightPage');
  }

  goWorkoutList() {
    this.navCtrl.push("GrowthHeightWorkoutPage");
  }

  goFoodList() {
    this.navCtrl.push("GrowthHeightFoodPage");
  }

  goSuitList() {
    this.navCtrl.push("GrowthHeightSuitPage");
  }

  goNoticeList() {
    this.navCtrl.push("GrowthHeightNoticePage");
  }

}
