import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrowthHeightPage } from './growth-height';

@NgModule({
  declarations: [
    GrowthHeightPage,
  ],
  imports: [
    IonicPageModule.forChild(GrowthHeightPage),
  ],
})
export class GrowthHeightPageModule {}
