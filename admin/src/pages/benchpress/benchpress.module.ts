import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BenchpressPage } from './benchpress';

@NgModule({
  declarations: [
    BenchpressPage,
  ],
  imports: [
    IonicPageModule.forChild(BenchpressPage),
  ],
})
export class BenchpressPageModule {}
