import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-benchpress',
  templateUrl: 'benchpress.html',
})
export class BenchpressPage {
  data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
      this.data = navParams.data;
      console.log(this.data);
      console.log(navParams.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BenchpressPage');
  }
 
}
