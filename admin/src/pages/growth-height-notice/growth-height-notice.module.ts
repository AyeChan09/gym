import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrowthHeightNoticePage } from './growth-height-notice';

@NgModule({
  declarations: [
    GrowthHeightNoticePage,
  ],
  imports: [
    IonicPageModule.forChild(GrowthHeightNoticePage),
  ],
})
export class GrowthHeightNoticePageModule {}
