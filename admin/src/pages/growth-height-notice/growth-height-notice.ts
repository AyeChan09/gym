import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'


@IonicPage()
@Component({
  selector: 'page-growth-height-notice',
  templateUrl: 'growth-height-notice.html',
})
export class GrowthHeightNoticePage {
  information: any[];
 
  constructor(
    public navCtrl: NavController, 
    private http: Http) {
    let localData = http.get('assets/json/growthHeightNotice.json').map(res => res.json().items);
    localData.subscribe(data => {
      this.information = data;
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad GrowthHeightNoticePage');
  }

   toggleSection(i) {
    this.information[i].open = !this.information[i].open;
  }
 
  toggleItem(i, j) {
    this.information[i].children[j].open = !this.information[i].children[j].open;
  }
 

}
