import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightLossPage } from './weight-loss';

@NgModule({
  declarations: [
    WeightLossPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightLossPage),
  ],
})
export class WeightLossPageModule {}
