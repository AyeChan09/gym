import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-weight-loss',
  templateUrl: 'weight-loss.html',
})
export class WeightLossPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightLossPage');
  }

  goWorkoutList() {
    this.navCtrl.push("WeightLossWorkoutPage");
  }

  goZumbaList() {
    this.navCtrl.push("WeightLossZumbaPage");
  }

  goFoodList() {
    this.navCtrl.push("WeightLossFoodPage");
  }

  goNoticeList() {
    this.navCtrl.push("WeightLossNoticePage");
  }

}
