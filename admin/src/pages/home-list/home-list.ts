import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage({
  name: 'home-list'
})
@Component({
  selector: 'page-home-list',
  templateUrl: 'home-list.html',
})
export class HomeListPage {
  reason: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
      this.reason = navParams.data;
      console.log('reason=>', this.reason);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeListPage');
  }

  viewDashboard() {
    this.navCtrl.setRoot("home");
  }

  goWorkoutList(data) {
    this.navCtrl.setRoot("workout-detail", data);
  }

  goWeightLossWorkoutList(data) {
    this.navCtrl.setRoot("weightlossworkout", data);
  }


}
