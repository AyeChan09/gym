import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-weight-gain-workout',
  templateUrl: 'weight-gain-workout.html',
})
export class WeightGainWorkoutPage {
  data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider) {
      this.apiService.getData_local('assets/json/weightGainData.json').subscribe(data => {
        this.data = data.data;
        console.log(this.data);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightGainWorkoutPage');
  }

  goBenchPress(data) {
    this.navCtrl.push("BenchpressPage", data);
  }

}
