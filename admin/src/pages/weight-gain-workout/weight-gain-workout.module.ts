import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightGainWorkoutPage } from './weight-gain-workout';

@NgModule({
  declarations: [
    WeightGainWorkoutPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightGainWorkoutPage),
  ],
})
export class WeightGainWorkoutPageModule {}
