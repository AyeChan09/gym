import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-weight-gain',
  templateUrl: 'weight-gain.html',
})
export class WeightGainPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightGainPage');
  }

  goWorkoutList() {
    this.navCtrl.push("WeightGainWorkoutPage");
  }

  goFoodList() {
    this.navCtrl.push("WeightGainFoodPage");
  }

  goNoticeList() {
    this.navCtrl.push("WeightGainNoticePage");
  }

}
