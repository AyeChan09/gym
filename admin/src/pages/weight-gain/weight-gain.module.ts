import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightGainPage } from './weight-gain';

@NgModule({
  declarations: [
    WeightGainPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightGainPage),
  ],
})
export class WeightGainPageModule {}
