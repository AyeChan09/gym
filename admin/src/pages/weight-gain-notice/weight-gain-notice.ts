import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'
 

@IonicPage()
@Component({
  selector: 'page-weight-gain-notice',
  templateUrl: 'weight-gain-notice.html',
})
export class WeightGainNoticePage  {
  information: any[];
 
  constructor(public navCtrl: NavController, private http: Http) {
    let localData = http.get('assets/json/weightGainNotice.json').map(res => res.json().items);
    localData.subscribe(data => {
      this.information = data;
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightGainNoticePage');
  }


   toggleSection(i) {
    this.information[i].open = !this.information[i].open;
  }
 
  toggleItem(i, j) {
    this.information[i].children[j].open = !this.information[i].children[j].open;
  }
 

}
