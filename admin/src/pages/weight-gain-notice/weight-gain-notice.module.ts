import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightGainNoticePage } from './weight-gain-notice';

@NgModule({
  declarations: [
    WeightGainNoticePage,
  ],
  imports: [
    IonicPageModule.forChild(WeightGainNoticePage),
  ],
})
export class WeightGainNoticePageModule {}
