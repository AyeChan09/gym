import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightGainFoodDetailsPage } from './weight-gain-food-details';

@NgModule({
  declarations: [
    WeightGainFoodDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightGainFoodDetailsPage),
  ],
})
export class WeightGainFoodDetailsPageModule {}
