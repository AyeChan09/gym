import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightLossFoodDetailsPage } from './weight-loss-food-details';

@NgModule({
  declarations: [
    WeightLossFoodDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightLossFoodDetailsPage),
  ],
})
export class WeightLossFoodDetailsPageModule {}
