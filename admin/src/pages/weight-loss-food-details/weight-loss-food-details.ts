import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-weight-loss-food-details',
  templateUrl: 'weight-loss-food-details.html',
})
export class WeightLossFoodDetailsPage {
data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
      this.data = navParams.data;
      console.log(this.data);
      console.log(navParams.data);
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightLossFoodDetailsPage');
  }

}
