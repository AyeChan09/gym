import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-growth-height-food',
  templateUrl: 'growth-height-food.html',
})
export class GrowthHeightFoodPage  {data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider,
    public storage: LocalStorageProvider) {
      this.apiService.getData_local('assets/json/growthHeightFoodData.json').subscribe(data => {
        this.data = data.data;
        this.addTick();
        console.log(this.data);
      });
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad GrowthHeightFoodPage');
  }
  addTick() {
    this.storage.getData('growth_height_tick').then(data => {
      let tickArr = data;
      console.log('tickArr=>', tickArr);
      if(tickArr == null) {
        this.data.forEach(element => {
          element['tick'] = false;
        })
      } else {
        this.data = tickArr;
      }
      console.log('data in addTick=>', this.data);
    });
  }
  goGrowthHeightFoodDetails(data) {
    this.storage.setData('growth_height_tick', this.data);
    this.navCtrl.push("GrowthHeightFoodDetailsPage", data);
  }
  ionViewWillLeave()
  {
    console.log('data in leave=>', this.data);
    this.storage.setData('growth_height_tick', this.data);
     console.log('leave');
  }

}
