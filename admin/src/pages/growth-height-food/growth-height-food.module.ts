import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrowthHeightFoodPage } from './growth-height-food';

@NgModule({
  declarations: [
    GrowthHeightFoodPage,
  ],
  imports: [
    IonicPageModule.forChild(GrowthHeightFoodPage),
  ],
})
export class GrowthHeightFoodPageModule {}
