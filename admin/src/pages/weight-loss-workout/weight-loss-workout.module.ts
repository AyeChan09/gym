import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightLossWorkoutPage } from './weight-loss-workout';

@NgModule({
  declarations: [
    WeightLossWorkoutPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightLossWorkoutPage),
  ],
})
export class WeightLossWorkoutPageModule {}
