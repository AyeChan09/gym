import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-weight-loss-workout',
  templateUrl: 'weight-loss-workout.html',
})
export class WeightLossWorkoutPage {
  data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private apiService: ApiServiceProvider) {
      this.apiService.getData_local('assets/json/weightLossData.json').subscribe(data => {
        this.data = data.data;
        console.log(this.data);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightLossWorkoutPage');
  }

  goWeightLossWorkoutDetails(data) {
    this.navCtrl.push("WeightLossWorkoutDetailsPage", data);
  }


}
