import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrowthHeightSuitPage } from './growth-height-suit';

@NgModule({
  declarations: [
    GrowthHeightSuitPage,
  ],
  imports: [
    IonicPageModule.forChild(GrowthHeightSuitPage),
  ],
})
export class GrowthHeightSuitPageModule {}
