import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightlossworkoutPage } from './weightlossworkout';

@NgModule({
  declarations: [
    WeightlossworkoutPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightlossworkoutPage),
  ],
})
export class WeightlossworkoutPageModule {}
