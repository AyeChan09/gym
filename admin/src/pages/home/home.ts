import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage({
  name: 'home'
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goWeightGain() {
    console.log('weight gain');
    this.navCtrl.push("WeightGainPage");
  }

  goWeightLoss() {
    console.log('weight gain');
    this.navCtrl.push("WeightLossPage");
  }

  goGrowthHeight() {
    console.log('weight gain');
    this.navCtrl.push("GrowthHeightPage");
  }
}
