import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-growth-height-food-details',
  templateUrl: 'growth-height-food-details.html',
})
export class GrowthHeightFoodDetailsPage{
data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
      this.data = navParams.data;
      console.log(this.data);
      console.log(navParams.data);
    }
    ionViewDidLoad() {
    console.log('ionViewDidLoad GrowthHeightFoodDetailsPage');
  }

}
