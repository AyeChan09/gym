import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrowthHeightFoodDetailsPage } from './growth-height-food-details';

@NgModule({
  declarations: [
    GrowthHeightFoodDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(GrowthHeightFoodDetailsPage),
  ],
})
export class GrowthHeightFoodDetailsPageModule {}
