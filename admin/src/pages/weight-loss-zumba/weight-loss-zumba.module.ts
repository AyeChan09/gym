import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightLossZumbaPage } from './weight-loss-zumba';

@NgModule({
  declarations: [
    WeightLossZumbaPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightLossZumbaPage),
  ],
})
export class WeightLossZumbaPageModule {}
