import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-weight-loss-zumba',
  templateUrl: 'weight-loss-zumba.html',
})
export class WeightLossZumbaPage {


video: any = {
        title: 'Zumba'
    };


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightLossZumbaPage');
  }

}
