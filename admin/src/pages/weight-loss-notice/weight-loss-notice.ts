import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'


@IonicPage()
@Component({
  selector: 'page-weight-loss-notice',
  templateUrl: 'weight-loss-notice.html',
})
export class WeightLossNoticePage {
  information: any[];
 
  constructor(public navCtrl: NavController, private http: Http) {
    let localData = http.get('assets/json/weightLossNotice.json').map(res => res.json().items);
    localData.subscribe(data => {
      this.information = data;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WeightLossNoticePage');
  }

   toggleSection(i) {
    this.information[i].open = !this.information[i].open;
  }
 
  toggleItem(i, j) {
    this.information[i].children[j].open = !this.information[i].children[j].open;
  }
 

}
