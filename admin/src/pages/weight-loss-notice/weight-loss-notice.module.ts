import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightLossNoticePage } from './weight-loss-notice';

@NgModule({
  declarations: [
    WeightLossNoticePage,
  ],
  imports: [
    IonicPageModule.forChild(WeightLossNoticePage),
  ],
})
export class WeightLossNoticePageModule {}
