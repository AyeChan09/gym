import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrowthHeightWorkoutDetailsPage } from './growth-height-workout-details';

@NgModule({
  declarations: [
    GrowthHeightWorkoutDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(GrowthHeightWorkoutDetailsPage),
  ],
})
export class GrowthHeightWorkoutDetailsPageModule {}
