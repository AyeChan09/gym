import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-growth-height-workout-details',
  templateUrl: 'growth-height-workout-details.html',
})
export class GrowthHeightWorkoutDetailsPage{
  data:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
      this.data = navParams.data;
      console.log(this.data);
      console.log(navParams.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GrowthHeightWorkoutDetailsPage');
  }



}
