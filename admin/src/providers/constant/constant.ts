import { Injectable } from '@angular/core';
import { AlertController,ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Injectable()
export class ConstantProvider {

  public serverPath = "http://arzaphosting2.biz/prototype/mma/arpos_backend/public/";
  public apiPath = this.serverPath+"posinternetordering/";

  // public serverPath = "http://192.168.1.4/";
  // public apiPath = this.serverPath+"WaiterApp/api.svc/";
  
  constructor(
      public alertCtrl: AlertController,
      public toastCtrl: ToastController) {
  }

  public getServerPath(){
    return this.serverPath;
  }

  public getApiPath(){
    return this.apiPath;
  }

  basicAlert(title,msg) {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: msg,
        buttons: ['OK']
      });
      alert.present();
  }

  confirmAlert(title,msg, text1, text2) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: title,
        message: msg,
        buttons: [
          {
            text: text2,
            role: 'cancel',
            handler: () => {
              resolve(false);
            }
          },
          {
            text: text1,
            handler: () => {
              resolve(true);
            }
          }
        ]
      });
      alert.present();
    });
  }

  toast(msg, position) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 2500,
        position: position
      });
      toast.present();
  }

  foodPickup: any;
}
