import { Http, Response, Headers, RequestOptions } from '@angular/http';
// import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { ConstantProvider } from '../constant/constant';

@Injectable()
export class ApiServiceProvider {

  apiPath = "";
  params = {};
  url = "";

  constructor(
    public http: Http,
    public constantProvider: ConstantProvider) {
    this.apiPath = this.constantProvider.getApiPath();
  }

  getData_local(json){
    return this.http.get(json).map(res => res.json());
  }
  
  getAllActivities(url) {
    this.url = url;
    // this.url = "activity/get_activity";
    // return this.get();
  }

  postActivities(url, params) {
    this.url = url;
    this.params = params;
    // this.url = "activity/save_photo?"
    // return this.post();
  }

  get(url, parameter = {}, headers = {}){
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    console.log(parameter, headers);
    let options = new RequestOptions({ headers : httpHeader, params:parameter  });
    console.log(this.apiPath+url);
    return this.http.get(this.apiPath + url, options)
    .map(this.extractData);    
  }

  update(url, parameter = {}, headers = {}){
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    console.log(parameter, headers);
    let options = new RequestOptions({ headers : httpHeader, params:parameter  });
    console.log(this.apiPath+url);
    return this.http.put(this.apiPath + url, options)
    .map(this.extractData);    
  }

  insert(url, parameter = {}, headers = {}){
    if(headers['Content-Type']!=="undefined"){
      headers['Content-Type'] = "application/json"
    }
    let httpHeader = new Headers(headers);
    console.log(parameter, headers);
    let options = new RequestOptions({ headers : httpHeader, params:parameter  });
    console.log(this.apiPath+url);
    return this.http.post(this.apiPath + url, options)
    .map(this.extractData);    
  }

  private extractData(res: Response) {
    return res.json()
  }

  // private catchError(error: Response | any) {
  //   console.log(error);
  //   return Observable.throw(error.json().error || "Server error.");
  // }


}
