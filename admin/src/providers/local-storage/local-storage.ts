import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class LocalStorageProvider {
  
  constructor(
    public http: HttpClient, public storage: Storage) {
      this.storage.ready().then(()=>{
      })
  }

  getNullSetData(name, value){
    return this.getData(name).then((data)=>{
      if (data == null){
        this.setData(name, value);
        return value;
      } else {
        return data;
      }
    })
  }

  setData(name, value){
    this.storage.set(name, value);
  }

  setDataThen(name, value){
    return this.storage.set(name, value);
  }

  getData(name){
    return this.storage.get(name);
  }

  checkData()
  {
    this.storage.keys().then(data=>{
      console.log('my keys: '+ data)
    })
  }

  clearData(){
    this.storage.clear();
  }

  removeData(name){
    this.storage.remove(name);
  }
}
